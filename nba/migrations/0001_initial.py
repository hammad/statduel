# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Team'
        db.create_table(u'nba_team', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('url', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'nba', ['Team'])

        # Adding model 'Season'
        db.create_table(u'nba_season', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('year', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'nba', ['Season'])

        # Adding model 'Player'
        db.create_table(u'nba_player', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('unique_name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('team', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nba.Team'])),
            ('jersey_number', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('height', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('date_of_birth', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('drafted', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('college', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'nba', ['Player'])

        # Adding model 'Game'
        db.create_table(u'nba_game', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('season', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nba.Season'])),
            ('home_team', self.gf('django.db.models.fields.related.ForeignKey')(related_name='home_game', to=orm['nba.Team'])),
            ('home_team_final_score', self.gf('django.db.models.fields.IntegerField')()),
            ('away_team', self.gf('django.db.models.fields.related.ForeignKey')(related_name='away_game', to=orm['nba.Team'])),
            ('away_team_final_score', self.gf('django.db.models.fields.IntegerField')()),
            ('post_season', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'nba', ['Game'])

        # Adding model 'PlayerStats'
        db.create_table(u'nba_playerstats', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nba.Player'])),
            ('team', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nba.Team'])),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['nba.Game'])),
            ('shots_attempted', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('shots_made', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('field_goal_percentage', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('three_pointers_attempted', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('three_pointers_made', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('three_point_percentage', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('free_throws_attempted', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('free_throws_made', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('free_throw_percentage', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('rebounds', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('assists', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('blocks', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('steals', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('personal_fouls', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('turnovers', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('points', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'nba', ['PlayerStats'])


    def backwards(self, orm):
        # Deleting model 'Team'
        db.delete_table(u'nba_team')

        # Deleting model 'Season'
        db.delete_table(u'nba_season')

        # Deleting model 'Player'
        db.delete_table(u'nba_player')

        # Deleting model 'Game'
        db.delete_table(u'nba_game')

        # Deleting model 'PlayerStats'
        db.delete_table(u'nba_playerstats')


    models = {
        u'nba.game': {
            'Meta': {'object_name': 'Game'},
            'away_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'away_game'", 'to': u"orm['nba.Team']"}),
            'away_team_final_score': ('django.db.models.fields.IntegerField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'home_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'home_game'", 'to': u"orm['nba.Team']"}),
            'home_team_final_score': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_season': ('django.db.models.fields.BooleanField', [], {}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['nba.Season']"})
        },
        u'nba.player': {
            'Meta': {'object_name': 'Player'},
            'college': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'drafted': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'height': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jersey_number': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['nba.Team']"}),
            'unique_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'weight': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'})
        },
        u'nba.playerstats': {
            'Meta': {'object_name': 'PlayerStats'},
            'assists': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'blocks': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field_goal_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'free_throw_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'free_throws_attempted': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'free_throws_made': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['nba.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personal_fouls': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['nba.Player']"}),
            'points': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rebounds': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'shots_attempted': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'shots_made': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'steals': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['nba.Team']"}),
            'three_point_percentage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'three_pointers_attempted': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'three_pointers_made': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'turnovers': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'nba.season': {
            'Meta': {'object_name': 'Season'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        u'nba.team': {
            'Meta': {'object_name': 'Team'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['nba']