from nba.models import Team, Game, Season
from bs4 import BeautifulSoup
from datetime import datetime, date
import requests


BASE_URL = 'http://espn.go.com/nba/team/schedule/_/name/{0}/year/{1}/seasontype/{2}/{3}'
REGULAR_SEASON = 2
PLAYOFFS = 3

def pull_games_for_season(season):
    _pull_games_for_season(season, REGULAR_SEASON)
    _pull_games_for_season(season, PLAYOFFS)

def _pull_games_for_season(season, season_type):
    match_id = []
    dates = []
    home_team = []
    home_team_score = []
    visit_team = []
    visit_team_score = []

    teams = Team.objects.all()

    for team in teams:
        url_info = team.url_info.split(',')
	get_url = BASE_URL.format(url_info[1], season, season_type, url_info[2])
	print get_url
        r = requests.get(get_url)
        soup = BeautifulSoup(r.text)
        if season_type == PLAYOFFS:
            print "searching for playoffs"
            navtab = soup.find(class_="mod-thirdnav-tabs")
            if not navtab:
                print "something went wrong"
                continue
            active_tab = navtab.ul.find(class_="active")
            if active_tab.string != "Postseason":
                print "no playoffs for {0} for {1} season".format(team.name, season)
                continue

        table = BeautifulSoup(r.text).table
        for row in table.find_all('tr')[1:]: # Remove header
            columns = row.find_all('td')
            try:
                boxscore_url_id = columns[2].a['href'].split('?id=')[1]
                score = columns[2].a.text.split(' ')[0].split('-')
                other_team_short = columns[1].find_all('a')[1]['href'].split('/')[-2]
                other_team = Team.objects.get(short_name=other_team_short)

                if columns[1].li.text == 'vs':
                    home_team = team
                    away_team = other_team
                else:
                    home_team = other_team
                    away_team = team

                if columns[2].span.text == 'W':
                    won = True
                else:
                    won = False

                if home_team == team:
                    if won:
                        home_team_score = score[0]
                        away_team_score = score[1]
                    else:
                        home_team_score = score[1]
                        away_team_score = score[0]
                else:
                    if won:
                        home_team_score = score[1]
                        away_team_score = score[0]
                    else:
                        home_team_score = score[0]
                        away_team_score = score[1]

                _d = datetime.strptime(columns[0].text, '%a, %b %d')
                if _d.month > 9:
                    year = season-1
                else:
                    year = season
                game_date = date(year, _d.month, _d.day)

                saved_season, created = Season.objects.get_or_create(year=season, name='{0}-{1}'.format(season-1, season))

                existing_games = Game.objects.filter(date=game_date, season=saved_season, home_team=home_team, home_team_final_score=home_team_score, away_team=away_team, away_team_final_score=away_team_score)

                if existing_games:
                    continue

                post_season = False
                if season_type == PLAYOFFS:
                    post_season = True

                game = Game(
                    date=game_date, 
                    season=saved_season,
                    home_team=home_team,
                    home_team_final_score=home_team_score,
                    away_team=away_team,
                    away_team_final_score=away_team_score,
                    post_season=post_season,
                    url_info='{0}'.format(boxscore_url_id)
                )
                print game
                game.save()
            except Exception as e:
                pass # Not all columns row are a match, is OK
                # print(e)
