from nba.models import Team
import requests
from bs4 import BeautifulSoup


def pull_teams():
    url = 'http://espn.go.com/nba/teams'
    r = requests.get(url)

    soup = BeautifulSoup(r.text)
    tables = soup.find_all('ul', class_='medium-logos')

    teams_urls = []
    for table in tables:
        lis = table.find_all('li')
        for li in lis:
            info = li.h5.a
            team_name = info.text.split()[-1]
            team_city = info.text.split()[:-1]
            team_city = ' '.join(team_city)
            url = info['href']
            prefix_1 = url.split('/')[-2]
            prefix_2 = url.split('/')[-1]
            try:
                existing_team = Team.objects.get(name=team_name, short_name=prefix_1)
            except Team.DoesNotExist:
                team = Team(
                    name=team_name, 
                    short_name=prefix_1,
                    city=team_city, 
                    url_info=','.join(
                        [url, prefix_1, prefix_2] 
                    )
                )
                print team
                team.save()
