import requests
from bs4 import BeautifulSoup
from datetime import datetime, date
from nba.models import Team, Player, PlayerStats, Game, PlayerImage
from nba.stats.one_offs import divide, true_shooting, effective_shooting, game_label
from django.core.files import File
import wikipedia
import urllib
import os

BASE_URL = 'http://espn.go.com/nba/boxscore?gameId={0}'
SEASON_ID = '1'

def pull_player_stats_from_games():
    games = Game.objects.all()
    for game in games:
        existing_stats = PlayerStats.objects.filter(game=game)
        if existing_stats:
            #print 'already have stats for this game!'
            continue
        print game
        request = requests.get(BASE_URL.format(game.url_info))
        table = BeautifulSoup(request.text, "lxml").find('table', class_='mod-data')
        try:
            heads = table.find_all('thead')
            bodies = table.find_all('tbody')
        except:
            print 'something went wrong'
            continue

        team_1_name = heads[0].th.text
        team_1_city = ' '.join(team_1_name.split()[:-1])
        team_1_name = team_1_name.split()[-1] 
        if team_1_city == "New Orleans" and team_1_name == "Hornets":
            team_1_name = "Pelicans"
        elif team_1_city == "Charlotte" and team_1_name == "Bobcats":
            team_1_name = "Hornets"
        team_1 = Team.objects.get(name=team_1_name, city=team_1_city)

        team_1_starters = bodies[0].find_all('tr')
        team_1_bench = bodies[1].find_all('tr')
        team_1_players = team_1_starters + team_1_bench

        for player in team_1_players:
            pull_stats(player, team_1, game)

        team_2_name = heads[3].th.text
        team_2_city = ' '.join(team_2_name.split()[:-1])
        team_2_name = team_2_name.split()[-1] 
        if team_2_city == "New Orleans" and team_2_name == "Hornets":
            team_2_name = "Pelicans"
        elif team_2_city == "Charlotte" and team_2_name == "Bobcats":
            team_2_name = "Hornets"
        team_2 = Team.objects.get(name=team_2_name, city=team_2_city)

        team_2_starters = bodies[3].find_all('tr')
        team_2_bench = bodies[4].find_all('tr')
        team_2_players = team_2_starters + team_2_bench

        for player in team_2_players:
            pull_stats(player, team_2, game)


def pull_player_images():
    players = Player.objects.all()
    for player in players:
        print player.name
        if PlayerImage.objects.filter(player=player).count() > 0:
            print 'Already have images for this player'
            continue
        try:
            wiki_player = wikipedia.page(player.name)
        except wikipedia.DisambiguationError as e:
            print e.options
            for option in e.options:
                search_term = None
                if 'basketball' in option.lower():
                    search_term = option
                    break
            if search_term:
                wiki_player = wikipedia.page(search_term)
        except:
            print 'didnt find this player'
            continue

        for image_url in wiki_player.images:

            print image_url
            if player.name.split()[0] in image_url:

                result = urllib.urlretrieve(image_url)
                player_image = PlayerImage(player=player)
                player_image.image.save(os.path.basename(image_url), File(open(result[0])))
                player_image.save()



def pull_stats(player, team, game):
    cols = player.find_all('td')
    player_name = cols[0].a.text
    player_position = cols[0].text.split(',')[1].strip()
    player_id = cols[0].a['href'].split('/')[-2]
    unique_name = '{0}{1}'.format(SEASON_ID, player_id)
    saved_player, created = Player.objects.get_or_create(
        unique_name=unique_name,
        name=player_name,
    )

    changed = False
    if saved_player.unique_name != unique_name:
        saved_player.unique_name = unique_name
        changed = True
    if saved_player.team != team:
        saved_player.team = team
        changed = True
    if saved_player.position != player_position:
        saved_player.position = player_position
        changed = True
    if changed:
        saved_player.save()
    try:
        existing_stats = PlayerStats.objects.get(player=saved_player, team=team, game=game)
        print 'stats already exist!'
    except PlayerStats.DoesNotExist:
        stats = PlayerStats(player=saved_player, team=team, game=game)
        played_game = True
        try:
            minutes = int(cols[1].text)
        except:
            minutes = 0
            played_game = False
        stats.minutes = minutes 

        if played_game:
            stats.shots_attempted = cols[2].text.split('-')[1]
            stats.shots_made = cols[2].text.split('-')[0]
            stats.field_goal_percentage = divide(stats.shots_made, stats.shots_attempted) * 100
            stats.three_pointers_attempted = cols[3].text.split('-')[1]
            stats.three_pointers_made = cols[3].text.split('-')[0]
            stats.three_point_percentage = divide(stats.three_pointers_made, stats.three_pointers_attempted) * 100
            stats.free_throws_attempted = cols[4].text.split('-')[1]
            stats.free_throws_made = cols[4].text.split('-')[0]
            stats.free_throw_percentage = divide(stats.free_throws_made, stats.free_throws_attempted) * 100
            stats.offensive_rebounds = cols[5].text
            stats.defensive_rebounds = cols[6].text
            stats.assists = cols[8].text
            stats.steals = cols[9].text
            stats.blocks = cols[10].text
            stats.turnovers = cols[11].text
            stats.personal_fouls = cols[12].text
            stats.plus_minus = cols[13].text
            stats.points = cols[14].text
            stats.true_shooting_percentage = true_shooting(stats)
            stats.effective_shooting_percentage = effective_shooting(stats)
            stats.game_label = game_label(stats)
        print stats
        stats.save()
