from django.contrib import admin
from nba.models import Player, Team, Game, Season, PlayerStats, PlayerImage, FeaturedPlayerImage

# Register your models here.
class GameAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'season', 
        'home_team', 
        'home_team_final_score', 
        'away_team', 
        'away_team_final_score', 
        'post_season', 
        'date',
    ) 
admin.site.register(Game, GameAdmin) 

class PlayerImageInline(admin.TabularInline):
    model = PlayerImage

class FeaturedPlayerImageInline(admin.TabularInline):
    model = FeaturedPlayerImage

class PlayerAdmin(admin.ModelAdmin):
    inlines = [PlayerImageInline, FeaturedPlayerImageInline]
    list_display = ('id', 'name', 'team') 
admin.site.register(Player, PlayerAdmin) 


class TeamGameInline(admin.TabularInline):
    model = Game
    fk_name = 'home_team'
    readonly_fields = [
        'season', 
        'home_team', 
        'home_team_final_score', 
        'away_team', 
        'away_team_final_score', 
        'post_season', 
        'date',
        'url_info'
    ]


class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'short_name') 
admin.site.register(Team, TeamAdmin) 


class PlayerStatsAdmin(admin.ModelAdmin):
    list_display = ('player', 'team', 'game') 
admin.site.register(PlayerStats, PlayerStatsAdmin) 


class SeasonGameInline(admin.TabularInline):
    model = Game
    fk_name = 'season'
    readonly_fields = [
        'season', 
        'home_team', 
        'home_team_final_score', 
        'away_team', 
        'away_team_final_score', 
        'post_season', 
        'date',
        'url_info'
    ]


class SeasonAdmin(admin.ModelAdmin):
    inlines = [SeasonGameInline,]
    list_display = ('id', 'year', 'name')
admin.site.register(Season, SeasonAdmin) 
