from django.db import models
from time import strftime


class Team(models.Model):
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=5)
    city = models.CharField(max_length=100)
    url_info = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{city} {name} ({short})'.format(city=self.city, name=self.name, short=self.short_name)


class Season(models.Model):
    year = models.IntegerField()
    name = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '{year}'.format(year=str(self.year))


class Player(models.Model):
    unique_name = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    team = models.ForeignKey(Team, blank=True, null=True)
    position = models.CharField(max_length=20, blank=True, null=True)
    jersey_number = models.CharField(max_length=1, blank=True, null=True)
    height = models.CharField(max_length=10, blank=True, null=True)
    weight = models.CharField(max_length=10, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    drafted = models.TextField(blank=True, null=True)
    college = models.TextField(blank=True, null=True)
    url_info = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{name} ({team})'.format(name=self.name, team=self.team.name)


def make_image_path(instance, original_name):
    return str(instance.player.id)+'/'+ strftime('%Y') + '/' + strftime('%m') + '/' + original_name


class PlayerImage(models.Model):
    player = models.ForeignKey(Player)
    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    image = models.ImageField(upload_to=make_image_path, height_field='height', width_field='width', null=True, blank=True)


class FeaturedPlayerImage(models.Model):
    player = models.ForeignKey(Player)
    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    image = models.ImageField(upload_to=make_image_path, height_field='height', width_field='width', null=True, blank=True)


class Game(models.Model):
    date = models.DateField()
    season = models.ForeignKey(Season)
    home_team = models.ForeignKey(Team, related_name='home_game')
    home_team_final_score = models.IntegerField()
    away_team = models.ForeignKey(Team, related_name='away_game')
    away_team_final_score = models.IntegerField()
    post_season = models.BooleanField()
    url_info = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{away_team} @ {home_team} {score1}-{score2} ({date})'.format(
            away_team=self.away_team.name, 
            home_team=self.home_team.name,
            score1=self.away_team_final_score,
            score2=self.home_team_final_score,
            date=self.date
        )


class PlayerStats(models.Model):
    player = models.ForeignKey(Player)
    team = models.ForeignKey(Team)
    game = models.ForeignKey(Game)
    game_label = models.CharField(max_length=50, blank=True, null=True)
    # Stat Sheet 
    true_shooting_percentage = models.IntegerField(blank=True, null=True)
    effective_shooting_percentage = models.IntegerField(blank=True, null=True)

    minutes = models.IntegerField(blank=True, null=True)
    shots_attempted = models.IntegerField(blank=True, null=True)
    shots_made = models.IntegerField(blank=True, null=True)
    field_goal_percentage = models.IntegerField(blank=True, null=True)
    three_pointers_attempted = models.IntegerField(blank=True, null=True)
    three_pointers_made = models.IntegerField(blank=True, null=True)
    three_point_percentage = models.IntegerField(blank=True, null=True)
    free_throws_attempted = models.IntegerField(blank=True, null=True)
    free_throws_made = models.IntegerField(blank=True, null=True)
    free_throw_percentage = models.IntegerField(blank=True, null=True)
    offensive_rebounds = models.IntegerField(blank=True, null=True)
    defensive_rebounds = models.IntegerField(blank=True, null=True)
    total_rebounds = models.IntegerField(blank=True, null=True)
    assists = models.IntegerField(blank=True, null=True)
    blocks = models.IntegerField(blank=True, null=True)
    steals = models.IntegerField(blank=True, null=True)
    personal_fouls = models.IntegerField(blank=True, null=True)
    turnovers = models.IntegerField(blank=True, null=True)
    plus_minus = models.IntegerField(blank=True, null=True)
    points = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return '({id}) {player}: {game}'.format(
            id=self.id,
            player=self.player,
            game=self.game
        )

    def save(self, *args, **kwargs):
        if self.offensive_rebounds is None:
            self.offensive_rebounds = 0
        if self.defensive_rebounds is None:
            self.defensive_rebounds = 0

        self.total_rebounds = int(self.offensive_rebounds) + int(self.defensive_rebounds)
        super(PlayerStats, self).save(*args, **kwargs)
