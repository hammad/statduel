from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from nba.stats.one_offs import populate_percentages 


class Command(BaseCommand):
    def handle(self, **options):
        #cache_full_season_stats_for_all_players()
        populate_percentages()
