from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from nba.stats.player_stats import cache_full_stats_for_all_players


class Command(BaseCommand):
    def handle(self, **options):
        #cache_full_season_stats_for_all_players()
        cache_full_stats_for_all_players()
