from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from nba.scrapers.teams import pull_teams


class Command(BaseCommand):
    def handle(self, **options):
        pull_teams()
