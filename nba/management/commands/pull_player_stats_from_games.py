from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from nba.scrapers.players import pull_player_stats_from_games


class Command(BaseCommand):
    def handle(self, **options):
        pull_player_stats_from_games() 
