from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from nba.scrapers.games import pull_games_for_season
from nba.scrapers.players import pull_player_stats_from_games


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--season',
        dest='season',
        action='store',
        help='Which season to pull'),
    ) 
    def handle(self, **options):
        self.season = options.get('season')
        if not self.season:
            print "No season designated"
            return
        pull_games_for_season(int(self.season))
        pull_player_stats_from_games()
