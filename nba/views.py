from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.core.cache import cache
from django.conf import settings
from json import dumps
from nba.models import Player, FeaturedPlayerImage, Season, PlayerStats
from datetime import datetime, date
from nba.stats.player_stats import tallied_stats, pull_values
import json

DTHANDLER = lambda obj: obj.isoformat() if (isinstance(obj, datetime) or isinstance(obj, date)) else json.JSONEncoder().default(obj)

def home(request):
    context = {
        'players_list': get_players_list,
    }
    return render_to_response('home.html', context)


def get_typeahead_list():
    all_players = Player.objects.all()
    typeahead = {}
    for player in all_players:
        typeahead[player.name] = player.unique_name
    return typeahead


def get_players_list():
    all_players = Player.objects.order_by('name').values('name', 'unique_name')
    return list(all_players)


def player_stats(request, players):
    context = {'populated': False}
    context['players_list'] = get_players_list() 
    players = players.split(',')
    if players:
        season = Season.objects.get(year=2015)
        context['player_infos'] = get_stats_for_players(players, season)
        context['page_title'] = render_title(context['player_infos'], ' - ')
        context['meta_description'] = render_title(context['player_infos'], ' vs. ')
        context['populated'] = True
    context['current_season'] = '2014-2015'
    context['last_season'] = '2013-2014'
    context['debug'] = settings.DEBUG
    return render_to_response('playerdash.html', context)


def render_title(players, join_slug):
    names = []    
    for player_info in players:
        names.append(player_info['player_name'])
    return join_slug.join(names)


def get_stats_for_players(player_ids, season):
    player_info_list = []
    for unique_id in player_ids:
        stats = get_stats_for_player(unique_id, season)
        player_info_list.append(stats)
    return player_info_list


def add_player(request, unique_name):
    season = Season.objects.get(year=2015)
    context = get_stats_for_player(unique_name, season)
    return HttpResponse(dumps(context))

def get_stats_for_player(unique_name, season):
    player = Player.objects.get(unique_name=unique_name)
    try:
        player_image = FeaturedPlayerImage.objects.get(player=player)
        image_url = player_image.image.url
    except FeaturedPlayerImage.DoesNotExist:
        image_url = '/static/images/default_court.jpg'

    statlines = PlayerStats.objects.filter(player=player, minutes__gt=0, game__season=season).order_by('game__date').reverse()
    result = {
        'totals': tallied_stats(statlines),
        'timeline': list(pull_values(statlines))
    }

    
    context = {
        'player_name': player.name,
        'team_name': "{0} {1}".format(player.team.city, player.team.name),
        'unique': unique_name,
        'player_image': image_url,
        'datasets': [
            {
                "label": "Current Season ({0})".format(season.name),
                "key": "current_season",
                'totals': dumps(result['totals']),
                'timeline': dumps(result['timeline'], default=DTHANDLER),
                'default_selected': "true",
            },
        ],
    }
    return context

def get_cached_stats_for_player(unique_name):
    player = Player.objects.get(unique_name=unique_name)
    try:
        player_image = FeaturedPlayerImage.objects.get(player=player)
        image_url = player_image.image.url
    except FeaturedPlayerImage.DoesNotExist:
        image_url = '/static/images/default_court.jpg'
    all_time_cache_key = '{0}_stats_for_all_time'.format(unique_name)
    last_season_cache_key = '{0}_stats_for_2013_season'.format(unique_name)
    current_season_cache_key = '{0}_stats_for_2014_season'.format(unique_name)
    fifteen_games_cache_key = '{0}_stats_for_last_15_games'.format(unique_name)
    ten_games_cache_key = '{0}_stats_for_last_10_games'.format(unique_name)
    five_games_cache_key = '{0}_stats_for_last_5_games'.format(unique_name)
    player_stats_all_time = cache.get(all_time_cache_key, None)
    player_stats_last_season = cache.get(last_season_cache_key, None)
    player_stats_current_season = cache.get(current_season_cache_key, None)
    player_stats_fifteen_games = cache.get(fifteen_games_cache_key, None)
    player_stats_five_games = cache.get(five_games_cache_key, None)

    DTHANDLER = lambda obj: obj.isoformat() if (isinstance(obj, datetime) or isinstance(obj, date)) else json.JSONEncoder().default(obj)

    context = {
        'players_list': dumps(get_typeahead_list(), default=DTHANDLER),
        'player_name': player.name,
        'team_name': "{0} {1}".format(player.team.city, player.team.name),
        'unique': unique_name,
        'player_image': image_url,
        'datasets': [
            {
                "label": "Last 5 Games",
                "key": "last_5",
                'totals': dumps(player_stats_five_games['totals']),
                'timeline': dumps(player_stats_five_games['timeline'], default=DTHANDLER) 
            },
            {
                "label": "Last 15 Games",
                "key": "last_15",
                'totals': dumps(player_stats_fifteen_games['totals']),
                'timeline': dumps(player_stats_fifteen_games['timeline'], default=DTHANDLER),
            },
            {
                "label": "Current Season (2013-2014)",
                "key": "current_season",
                'totals': dumps(player_stats_current_season['totals']),
                'timeline': dumps(player_stats_current_season['timeline'], default=DTHANDLER),
                'default_selected': "true",
            },
            {
                "label": "Last Season (2012-2013)",
                "key": "last_season",
                'totals': dumps(player_stats_last_season['totals']),
                'timeline': dumps(player_stats_last_season['timeline'], default=DTHANDLER),
            },
            {
                "label": "All Time",
                "key": "all_time",
                'totals': dumps(player_stats_all_time['totals']),
                'timeline': dumps(player_stats_all_time['timeline'], default=DTHANDLER),
            },

        ]
    }
    return context
