current_season = "{{current_season}}";
last_season = "{{last_season}}";


allData = [
{% if populated %}

{% for player in player_infos %}
{
    name: "{{player.player_name}}",
    image: "{{player.player_image}}",
    team_name: "{{player.team_name}}",
    unique_name: "{{player.unique}}",
    dataSets: {{player.datasets | safe}},
},
{% endfor %}
{% endif %}
];

scoringGraphs = [];
scoringTimelineGraphs = [];

playmakingGraphs = [];
playmakingTimelineGraphs = [];

_scoringData = {
    "TRUE SHOOTING": {
        "active": true,
        "players": []
    },
    "EFFECTIVE FGs": {
        "active": true,
        "players": []
    },
    "SHOOTING": {
        "active": true, 
        "players": []
    },
    "FREE THROWS": {
        "active": true,
        "players": []
    },
    "3 POINTERS": {
        "active": true,
        "players": []
    }
}

_playmakingData = {
    "Points": {
        "active": true, 
        "players": []
    },
    "Rebounds": {
        "active": true, 
        "players": []
    },
    "Assists": {
        "active": true,
        "players": []
    },
    "Blocks": {
        "active": true,
        "players": []
    },
    "Steals": {
        "active": true,
        "players": []
    },
    "Plus/Minus": {
        "active": true,
        "players": []
    }
}

BREAKDOWN_DICT = {
    "field_goal_percentage": {
        "attempted": "shots_attempted",
        "made": "shots_made", 
    },
    "true_shooting_percentage": {
        "attempted": "shots_attempted",
        "made": "shots_made", 
    },
    "effective_shooting_percentage": {
        "attempted": "shots_attempted",
        "made": "shots_made", 
    },
    "three_point_percentage": {
        "attempted": "three_pointers_attempted",
        "made": "three_pointers_made", 
    },
    "free_throw_percentage": {
        "attempted": "free_throws_attempted",
        "made": "free_throws_made", 
    },
};

scoringChart = null;
scoringTimelineChart = null;
playmakingChart = null;
playmakingTimelineChart = null;
