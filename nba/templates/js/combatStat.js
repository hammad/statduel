(function (){

var History = window.History;
if (!History.enabled){
}

History.Adapter.bind(window,'popstate',function() {
    console.log("CHANGING THE URL");
});

PALETTE = [
    { color: "#225B66", inverse: "#EEEEEE" },//aqua blue
    { color: "#17A3A5", inverse: "#EEEEEE" }, //lighter blue
    { color: "#8DBF67", inverse: "#EFEFEF" }, //pastel green
    { color: "#FCCB5F", inverse: "#000000" }, //bearable yellow
    { color: "#FC6E59", inverse: "#EFEFEF" } //shirt pink
];


var $select = $('#player-select').selectize({
    selectOnTab: true,
    persist: false,
    closeAfterSelect: true,
    allowEmptyOption: false,
    onItemAdd: addPlayerDefault,
    hideSelected: true,
    openOnFocus: false,
    onInitialize: function() {
        $('.right-inner-addon').show();
    },
});
var $select_mobile = $('#player-select-mobile').selectize({
    selectOnTab: true,
    persist: false,
    closeAfterSelect: true,
    allowEmptyOption: false,
    onItemAdd: addPlayerMobile,
    hideSelected: true,
    openOnFocus: false,
    onInitialize: function() {
        $('.right-inner-addon').show();
    },
});

var control = $select[0].selectize;
var control_mobile = $select_mobile[0].selectize;

function addPlayerDefault() {
    return addPlayer(control);
}

function addPlayerMobile() {
    return addPlayer(control_mobile);
}

function addPlayer(control){
    var unique_id = control.getValue();
    $.get( "/nba/addplayer/"+unique_id)
        .done(function (data) {
            var stats = JSON.parse(data);
            var playerData = formatPlayerData(stats);
            allData.push(playerData);
            loadPlayerCard(playerData);
            renderPlayerGraphs(playerData);
            control.clear();
            updatePlayerURL();
            //$("#typeahead").typeahead('val', '');
            //$('#typeahead').focus();
        });
    return true;
}

function updatePlayerURL(){
    var player_ids_list = getActivePlayerIDString()
    History.pushState(null, '', player_ids_list);
}

function getActivePlayerIDString(){
    var list = [];
    for (var i=0; i<allData.length; i++){
        list.push(allData[i].unique_name);
    }
    return list.join();
}

function formatPlayerData(stats){
    var dict = {};
    dict.name = stats.player_name;
    dict.image = stats.player_image;
    dict.team_name = stats.team_name;
    dict.unique_name = stats.unique;
    dict.dataSets = stats.datasets;
    return dict;
}


function getTimeRangeKey(player_id){
   // var selector = 'time-select-'+player_id;
   // var rangeSelect = $(selector);
   // getSelectedOptionValue(player_id)

    var active = $('.rangeselect'+unique+'.active');
    var index = active.data('index');
    if (typeof(index) === 'undefined'){
        index = 2;
    }
    return "timeRange" + index; 
}

$('.scoring-toggle').click(function() {
    $(this).toggleClass('label-default').toggleClass('label-primary');  
    renderPercentagesGraphs();
});

$('.timeline-scoring-toggle').click(function() {
    $('.timeline-scoring-toggle').removeClass('label-primary').addClass('label-default');
    $(this).toggleClass('label-default').toggleClass('label-primary');  
    renderTimelinePercentagesGraph();
    //renderLinePercentageGraphs();
});

$('.timeline-playmaking-toggle').click(function() {
    $('.timeline-playmaking-toggle').removeClass('label-primary').addClass('label-default');
    $(this).toggleClass('label-default').toggleClass('label-primary');  
    renderTimelinePlaymakingGraph();
    //renderLinePercentageGraphs();
});

$('.playmaking-toggle').click(function() {
    $('.line-playmaking-toggle').removeClass('label-primary').addClass('label-default');
    $(this).toggleClass('label-default').toggleClass('label-primary');  
    renderPlaymakingGraphs();
});

$('.toggle-nav-tab').click(function() {
    var tab = $(this).data('tab');
    var current = $(".nav-tab.active-tab");
    current.toggleClass('active-tab');
    current.fadeOut(100, function() {
        $("#"+tab).fadeIn(200);
        $("#"+tab).toggleClass('active-tab');
    });
});

$('#add-player-empty-plus').click(function() {
    $('.search-bar-container').effect("shake", { times: 1, distance: 5, direction: "up" }, 300);
    control.focus();
});

$('#player_box_add_player').click(function() {
    control_mobile.focus();
});

$('#main-logo').click(function() {
    window.location.href = '/';
});

Date.prototype.addHours = function(h){
    this.setHours(this.getHours()+h);
    return this;
}

$('.btn-group').button();

{% include "js/dataController.js" %}
{% include "js/renderGraphs.js" %}


$("a[title$='JavaScript charts']").hide()

var config = {
    networks: {
        facebook: {
            before: function(element) {
                scoringChart.AmExport.output({format: "jpg", output: "datastring" }, function(data) {
                    var image = encodeURIComponent(data);
                    this.image = image;
                });
            }
        }
    }
};
//$("#share-percentages-graph").share(config);
//new Share("#share-percentages-graph", config);

})();


