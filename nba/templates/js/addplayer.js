var matcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        matches = [];

        substrRegex = new RegExp(q, 'i');

        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                matches.push({ value: str });
            }
        });

        var limit = 4;
        if (matches.length < limit){
            cb(matches);
        } else {
            cb(matches.slice(0, limit));
        }
    };
}; 

var players = {{ players_list|safe }};

console.log("HELLO");

$("#typeahead").typeahead(
{
    hint: true,
    highlight: true,
    minLength: 1,
    autoselect: true,
},
{
    name: 'players',
    source: matcher(Object.keys(players)),
    displayKey: 'value'
});

$('#typeahead').keypress(function (e) {
    if (e.which == 13) {
        console.log('clicked ENTER!');
        $('#plus-button').click();
        return true;
    }
});


$("#plus-button").on('click', function (e) {
    var searchTerm = $("#typeahead").typeahead('val');
    var uniqueId = players[searchTerm];
    delete players[searchTerm];
    console.log(uniqueId);
    var url = "http://192.241.185.245:9091/nba/addplayer/?unq=" + uniqueId;
    $.get( "http://192.241.185.245:9091/nba/addplayer/", { unq: uniqueId } )
        .done(function (data) {
            var stats = JSON.parse(data);
            var newPlayerGraph = {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "fillAlphas": 1,
                "id": "AmGraph-2",
                "title": stats.player_name,
                "type": "column",
                "valueField": stats.player_name 
            };
            percentagesData.graphs.push(newPlayerGraph);
            percentagesData.dataProvider[0][stats.player_name] = stats.fg_percentage.toString(); //shooting %
            percentagesData.dataProvider[1][stats.player_name] = stats.ft_percentage.toString(); //free throws
            percentagesData.dataProvider[2][stats.player_name] = stats.three_pt_percentage.toString(); //3 pointers
            //var newPercentageSeries = {
            //    type: "column",
            //    showInLegend: true,
            //    name: stats.player_name,
            //    dataPoints: [
            //        { label: "Field Goals", y: stats.fg_percentage },
            //        { label: "3 Pointers", y: stats.three_pt_percentage },
            //        { label: "Free Throws", y: stats.ft_percentage },
            //    ],
            //};
            //var newPlaymakingSeries = {
            //    type: "column",
            //    showInLegend: true,
            //    name: stats.player_name,
            //    dataPoints: [
            //        { label: "Assists", y: stats.assists },
            //        { label: "Blocks", y: stats.blocks },
            //        { label: "Steals", y: stats.steals },
            //        { label: "Rebounds", y: stats.rebounds },
            //    ],
            //};
            //percentageData.push(newPercentageSeries);
            //playmakingData.push(newPlaymakingSeries);
            //percentageChart.render();
            //playmakingChart.render();
            //var playerTab = $( "<div/>", { "class": "col-lg-3 center"}).css("display", "none").appendTo( "#player-row" );
            //playerTab.html("Kevin Durant<br>Position: SF<br>Age:27");
            //playerTab.toggle("slide", {direction: "right"}, 500);
            //var playerTab = $( "<div/>", { "class": "col-lg-3 center"}).css("display", "none").appendTo( "#player-row" );
            //playerTab.html("LEBRON JAMES<br>Position: PF<br>Age:28");
        });
 

});
console.log("GOODBYE");
