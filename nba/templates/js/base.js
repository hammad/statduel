function onPlayerSelected() {
    var unique_id = control.getValue();
    window.location.href = "/nba/playerstats/"+unique_id;
}

var $select = $('#player-select').selectize({
    selectOnTab: true,
    persist: false,
    closeAfterSelect: true,
    allowEmptyOption: false,
    onChange: onPlayerSelected,
    onInitialize: function() {
        $('.right-inner-addon').show();
    },
});

var control = $select[0].selectize;

$('#welcome-text').fadeIn(1000);
