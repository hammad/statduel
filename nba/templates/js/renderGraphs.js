/*** First Chart in Dashboard page / Second Chart in Charts.html ***/

$(document).ready(function(){
    renderAll();
});

USED_COLORS = [];

function getGradientStyle(color){
    var gradientTemplate = $('#player_card_color_template').html();
    Mustache.parse(gradientTemplate);
    var info = {
        "m_color": color 
    }
    var rendered = Mustache.render(gradientTemplate, info);
    return rendered;
}

function pickPaletteColor(){
    var randomIndex = Math.floor(Math.random() * PALETTE.length);
    var color = PALETTE[randomIndex];
    USED_COLORS.push(color);
    PALETTE.splice(randomIndex, 1);
    return color;
}

function loadPlayerCard(playerData){
    var colorObject = pickPaletteColor();
    var color = colorObject.color;
    var inverseColor = colorObject.inverse;
    playerData.color = color;
    playerData.inverseColor = inverseColor;
    var template = $('#player_card_template').html();
    Mustache.parse(template);
    var info = {
        "m_player_name": playerData.name,
        "m_player_team": playerData.team_name,
        "m_unique_id": playerData.unique_name,
        "m_unique_style": getGradientStyle(color),
        "m_player_image": playerData.image,
        "m_current_season_name": current_season,
        "m_last_season_name": last_season,
        "m_color": color,
        "m_datasets": playerData.dataSets,
    };
    var rendered = Mustache.render(template, info);
    //$('.sidebar-nav').append(rendered);
    $('#add-player-empty-plus').before(rendered);
    loadPlayerBox(info);
    initializeSelect(playerData.unique_name);

    $(".player_card#" + playerData.unique_name).click(function(e) {
      if($(e.target).is('.remove-player span')){
          e.preventDefault();
          removePlayer(playerData);
          return;
      }
      var title = $(this);
      var list = title.parent();
      var content = list.children(".playerInfo");
      content.slideToggle(300, function() {
      });
    });
    //removeNameFromTypeahead(playerData.name);
}

function loadPlayerBox(info){
    var template = $('#player_box_template').html();
    Mustache.parse(template);
    var rendered = Mustache.render(template, info);
    $('#player_box_add_player').before(rendered);
}

function initializeSelect(player_id) {
    var selector = '#time-select-'+player_id;
    $(selector).selectpicker();
    $(selector).change(function() { 

        //var option = $(this).children(;
    });
}

function getSelectedTimeRangeKey(player_id){
    var selector = '#time-select-'+player_id;
    var select = $(selector);
    var value = select.val();
    var option = select.children('option:contains("'+value+'")');
    var dataVal = option.data('value');
    return dataVal;

}

function removePlayer(playerData){
    var elementPos = allData.map(function(x) {return x.unique_name}).indexOf(playerData.unique_name);
    allData.splice(elementPos, 1);

    elementPos = scoringGraphs.map(function(x) {return x.id}).indexOf('graph-'+playerData.unique_name);
    scoringGraphs.splice(elementPos, 1);

    elementPos = playmakingGraphs.map(function(x) {return x.id}).indexOf('graph-'+playerData.unique_name);
    playmakingGraphs.splice(elementPos, 1);
    updatePlayerURL();

    //addNameToTypeahead(playerData);
    var playerCard = $('.player_card#'+playerData.unique_name);
    var list = playerCard.parent();
    list.remove();
    renderAllGraphs();
}

function addNameToTypeahead(playerData){
}

function refreshTypeahead(){
    deleteTypeahead();
    initializeTypeahead();
}


function renderAll(){
    for (var i=0; i < allData.length; i++){
        var playerData = allData[i]; 
        if (playerData.name != "League Average"){
            loadPlayerCard(playerData);
        }
        renderPlayerGraphs(playerData);
    }
}

function renderAllGraphs(){
    for (var i=0; i < allData.length; i++){
        var playerData = allData[i]; 
        renderPlayerGraphs(playerData);
    }
}

function renderPercentagesGraphs(){
    for (var i=0; i < allData.length; i++){
        var playerData = allData[i]; 
        renderPercentagesGraph(playerData);
    }
}

function renderPlaymakingGraphs(){
    for (var i=0; i < allData.length; i++){
        var playerData = allData[i]; 
        renderPlaymakingGraph(playerData);
    }
}

function getExportConfig() {
    exportConfig = {
        "enabled": true,
        "libs": {
            "path": "/static/js/amcharts/plugins/export/libs/"
        },
        //"menu": [ {
        //    "class": "export-main",
        //    "menu": [ {
        //        "label": "Download as Image",
        //        "menu": ["PNG", "JPG", "SVG" ]
        //    //}, {
        //        //"label": "Share",
        //        //"menu": ["Facebook", "Twitter"]
        //    }, {
        //        "label": "Annotate",
        //        "action": "draw",
        //        "menu": [ {
        //            "class": "export-drawing",
        //            "menu": [ //{
        //                //"label": "Download as Image",
        //                //"menu": ["PNG", "JPG", "SVG" ]
        //                "PNG", "JPG", "SVG" 
        //            ],
        //            //}, ]//{
        //                //"label": "Share",
        //                //"menu": ["FB", "Twitter"]
        //            //}]
        //        }]
        //    }],
        //} ],
    }
    return exportConfig;
}

function pushGraph(playerData, graphArray, formatter){
    var elementPos = graphArray.map(function(x) {return x.id}).indexOf('graph-'+playerData.unique_name);
    if (elementPos === -1){
        var newGraph = {
            balloonText: formatter("[[title]]: [[value]]"),
            title: playerData.name,
            id: "graph-" + playerData.unique_name,
            valueField: playerData.name,
            precision: 1,
        };
        if (playerData.unique_name !== 'league-average'){
            newGraph.type = "column";
            newGraph.fillAlphas = 1;
            newGraph.labelText = formatter("[[value]]");
            newGraph.labelPosition = "middle";
            //newGraph.showAllValueLabels = true;
            //newGraph.columnWidth = 0.5;
            newGraph.colorField = playerData.unique_name+"color";
            newGraph.lineColorField = playerData.unique_name+"color";
            newGraph.lineColor = playerData.color;
            newGraph.color = playerData.inverseColor;
            newGraph.showHandOnHover = true;
        } else {
            newGraph.type = "step";
            newGraph.lineColor = "#786F6F";
            newGraph.noStepRisers = true;
            newGraph.lineThickness = 2;
            //newGraph.columnWidth = 0.75;
        }   
        if (typeof(playerData.color) !== "undefined"){
            //newGraph.colorField = "color";
        }
        graphArray.push(newGraph);
    }
}

function getTimeRangeForPlayer(playerData){
    var timeRangeKey = getSelectedTimeRangeKey(playerData.unique_name);
    var timeRangeIndex = playerData.dataSets.map(function(x) {return x.key}).indexOf(timeRangeKey);
    var timeRange = playerData.dataSets[timeRangeIndex];
    return timeRange;
}

function renderPercentagesGraph(playerData){
    var timeRange = getTimeRangeForPlayer(playerData);

    var totals = JSON.parse(timeRange.totals);

    pushGraph(playerData, scoringGraphs, function(s) { return s+"%"; });

    var activeScoringToggles = $(".stat-toggle-label.scoring-toggle");
    for (var i=0; i<activeScoringToggles.length; i++){
        var toggle = $(activeScoringToggles[i]);
        var category = toggle.attr("data-category");
        var statKey = toggle.attr("data-value");
        var newPlayer = {};
        newPlayer[playerData.name] = totals[statKey];
        newPlayer[playerData.unique_name+"color"] = playerData.color;

        var exists = false;
        for(var j = 0;j < _scoringData[category]["players"].length; j++){
            if (_scoringData[category].players[j].hasOwnProperty(playerData.name)){
                _scoringData[category].players[j][playerData.name] = totals[statKey]
                exists = true;
                break;
            }
        }
 
        if(!exists){
            _scoringData[category]["players"].push(newPlayer);
        }


        if (toggle.hasClass("label-primary")){
            _scoringData[category]["active"] = true;
        } else {
            _scoringData[category]["active"] = false;
        }
    }

    tempData = [];
    for (var key in _scoringData){
        if (_scoringData[key].active){
            var data = {};
            data["category"] = key;
            data["subdata"] = [];
            //data["color"] = getColor();
            for (var i=0;i<_scoringData[key].players.length;i++){
                var player = _scoringData[key].players[i];
                $.extend(data, player); 
            }
            tempData.push(data);
        }
    }
    if (scoringChart === null){
        scoringChart = AmCharts.makeChart("scoring-chart",
            {
                "type": "serial",
                //"pathToImages": "http://cdn.amcharts.com/lib/3/images/",
                "categoryField": "category",
                "fontFamily": "Lato",
                "fontSize": 12,
                "startDuration": 1,
                "theme": "none",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "fontSize": 14,
                },
                "trendLines": [],
                "graphs": scoringGraphs,
                "guides": [],
                "valueAxes": [
                    {
                        //"gridPosition": "start",
                        "maximum": 100,
                        "minimum": 0,
                        "gridAlpha": 0,
                        "axisAlpha": 0,
                        "fillAlpha": 0.05,
                        "fillColor": "#000000",
                        "tickLength": 0,
                        "autoGridCount": false,
                        "gridCount": 10,
                        "labelsEnabled": true,
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "legend": {
                    "useGraphSettings": true,
                    "align": "center",
                    "borderAlpha": .25,
                    //"data": [{title: "One", color: "#3366CC"},{title: "Two", color: "#EEEFFF"}]
                },
                "titles": [
                    {
                        "size": 22,
                        "text": "PERCENTAGES"
                    }
                ],
                "dataProvider": tempData,
                "panEventsEnabled": false,
                "export": getExportConfig(),
                "responsive": {
                    "enabled": true,
                    "rules": [
                        {
                            "maxWidth": 500,
                            "overrides": {
                                "categoryAxis": {
                                    "labelRotation": 80,
                                    "tickLength": 2,
                                    "fontSize": 12,
                                    "labelOffset": 10,
                                },
                                "valueAxes": {
                                    "fontSize": 0,
                                },
                            },
                        },
                    ],
                },
            });

        scoringChart.addListener("clickGraphItem", function (event) {
            // let's look if the clicked graph item had any subdata to drill-down into
            // getting player id 
            var player_id = event.graph.id.substring(6);
            var category = event.item.category;
            TIMELINE_PERCENTAGES_SELECTED_PLAYER = player_id;
            //category_toggle.click();
            var selector = ".timeline-scoring-toggle[data-category='" + category + "']";
            $('.timeline-scoring-toggle').removeClass('label-primary').addClass('label-default');
            var category_toggle= $(selector).removeClass('label-default').addClass('label-primary');
            var category_value = $(selector).data('value');

            showTimelinePercentagesView();
            renderTimelinePercentagesGraph(player_id);
            //renderLinePercentageGraphs(player_id, category);
            //showPercentagesLineChart();
            //event.chart.dataProvider = event.item.dataContext.subdata;
            //event.chart.validateData();
        });

    } else {
        scoringChart.dataProvider = tempData;
        scoringChart.graphs = scoringGraphs;
        scoringChart.validateData();
        scoringChart.animateAgain();
    }
}

TIMELINE_PERCENTAGES_SELECTED_PLAYER = null;
TIMELINE_PLAYMAKING_SELECTED_PLAYER = null;

function showTimelinePlaymakingView(){
    $("#playmaking-chart").hide();
    $("#playmaking-toggles").hide();
    $("#timeline-playmaking-chart").show();
    $("#timeline-playmaking-toggles").show();
}

function showTimelinePercentagesView(){
    $("#scoring-chart").hide();
    $("#scoring-toggles").hide();
    $("#timeline-scoring-chart").show();
    $("#timeline-scoring-toggles").show();
}

function getPlayerData(player_id){
    var elementPos = allData.map(function(x) {return x.unique_name}).indexOf(player_id);
    var playerData = allData[elementPos];
    return playerData;
}


function renderTimelinePlaymakingGraph(player_id){
    if (player_id === undefined){
        player_id = TIMELINE_PLAYMAKING_SELECTED_PLAYER;
    }
    var playerData = getPlayerData(player_id);
    var timeRange = getTimeRangeForPlayer(playerData);
    var timeline = JSON.parse(timeRange.timeline);

    var active_toggle = $('.timeline-playmaking-toggle.label-primary');
    //var category = active_toggle
    var category_value = active_toggle.data('value');

    var tempData = [];

    for (var i=0; i<timeline.length;i++){
        var game = timeline[i];
        var gameEntry = {};
//        gameEntry['label'] = game.game_label;
        //var dateString = game.game_label.split(" ")[0];
        var dateString = game.game__date;
        //gameEntry['date'] = AmCharts.stringToDate(dateString, "M/DD");
        gameEntry['label'] = game.game_label;
        gameEntry['date_raw'] = dateString;
        gameEntry['date'] = new Date(dateString).addHours(4);
        gameEntry['value'+playerData.unique_name] = game[category_value];
        tempData.push(gameEntry);
    }

    tempData.sort(function(a,b){
        return a.date - b.date;
    });


    var newGraph = {
        "balloonText": "<b>[[value]]<b> [[label]]<br>[[date_raw]]",
        "bullet": "round",
        "bulletSize": 10,
        "id": "timeline-playmaking-graph-" + playerData.unique_name,
        "title": playerData.name,
        "lineColor": playerData.color,
        "lineThickness": 2,
        "valueField": "value"+playerData.unique_name,
        "type": "smoothedLine",
        //"yField": "value"+playerData.unique_name
    }
    //scoringLineGraphs.push(newGraph);


    if (playmakingTimelineChart === null){
        playmakingTimelineChart = AmCharts.makeChart("timeline-playmaking-chart", {
            "type": "serial",
            "categoryField": "date",
            //"startDuration": 1,
            //"percentPrecision": 0,
            "fontFamily": "Lato",
            "fontSize": 14,
            //"precision": 0,
            "categoryAxis": {
                    "tickLength": 0,
                    "labelRotation": 36,
                    "parseDates": true,
                    "fontSize": 14,
                    "autoGridCount": true,
                    "centerLabelOnFullPeriod": true,
                    "minPeriod": "hh",
                    "labelOffset": 10,
            },
            "trendLines": [
            ],
            "graphs": [newGraph],
            "valueAxes": [
                {
                    "gridAlpha": 0,
                    "axisAlpha": 0,
                    "fillAlpha": 0.05,
                    "fillColor": "#000000",
                    "tickLength": 0,
                    "labelsEnabled": true,
                }
            ],
            "dataDateFormat": "YYYY-MM-DD HH:NN",
            "allLabels": [{
                "text": "< Go Back",
                "bold": true,
                "x": 15,
                "y": 15,
                "size": 18,
                "align:": "left",
                "url": "#",
            }],
            //"balloon": {},
            "legend": {
                    "useGraphSettings": true,
                    "align": "center",
                    "borderAlpha": .25,
            },
            "titles": [
                {
                    "size": 22,
                    //"text": playerData.name+" ("+category+")",
                    "text": playerData.name,
                }
            ],
            "mouseWheelZoomEnabled": true,
            "dataProvider": tempData,
            "export": getExportConfig(),
            "panEventsEnabled": false,
            "responsive": {
                "enabled": true,
                "rules": [
                    {
                        "maxWidth": 300,
                        "overrides": {
                            "categoryAxis": {
                                "labelRotation": 20,
                            }
                        }
                    },
                ],
            },
        });

        $('#timeline-playmaking-chart tspan:contains("Go Back")').click(function() { 
            goBackPlaymaking();  
        });

        // this method is called when chart is first inited as we listen for "dataUpdated" event
        
    } else {
        //scoringLineChart.titles[0].text = playerData.name,
        playmakingTimelineChart.graphs = [newGraph];
        playmakingTimelineChart.dataProvider = tempData;
        //scoringLineChart.validateNow();
        playmakingTimelineChart.validateData();
        playmakingTimelineChart.animateAgain();
        $('#timeline-playmaking-chart tspan:contains("Go Back")').click(function() { 
            goBackPlaymaking();  
        });
    }
}

function zoomChart(chart) {
    // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
    chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
}


function renderTimelinePercentagesGraph(player_id){
    if (player_id === undefined){
        player_id = TIMELINE_PERCENTAGES_SELECTED_PLAYER;
    }
    var playerData = getPlayerData(player_id);
    var timeRange = getTimeRangeForPlayer(playerData);
    var timeline = JSON.parse(timeRange.timeline);

    var active_toggle = $('.timeline-scoring-toggle.label-primary');
    //var category = active_toggle
    var category_value = active_toggle.data('value');
    var breakdown_data = BREAKDOWN_DICT[category_value];
    var breakdown_attempted = breakdown_data.attempted;
    var breakdown_made = breakdown_data.made;

    var tempData = [];

    for (var i=0; i<timeline.length;i++){
        var game = timeline[i];
        var gameEntry = {};
//        gameEntry['label'] = game.game_label;
        //var dateString = game.game_label.split(" ")[0];
        var dateString = game.game__date;
        //gameEntry['date'] = AmCharts.stringToDate(dateString, "M/DD");
        gameEntry['label'] = game.game_label;
        gameEntry['date_raw'] = dateString;
        gameEntry['date'] = new Date(dateString).addHours(4);
        gameEntry['value'+playerData.unique_name] = game[category_value];
        gameEntry['breakdown'] = game[breakdown_made] + " of " + game[breakdown_attempted];
        tempData.push(gameEntry);
    }

    tempData.sort(function(a,b){
        return a.date - b.date;
    });


    var newGraph = {
        "balloonText": "<b>[[value]]%<b> [[label]]<br>[[breakdown]]",
        "bullet": "round",
        "bulletSize": 10,
        "id": "line-graph-" + playerData.unique_name,
        "title": playerData.name,
        "lineColor": playerData.color,
        "lineThickness": 2,
        "valueField": "value"+playerData.unique_name,
        "type": "smoothedLine",
        //"yField": "value"+playerData.unique_name
    }
    //scoringLineGraphs.push(newGraph);


    if (scoringTimelineChart === null){
        scoringTimelineChart = AmCharts.makeChart("timeline-scoring-chart", {
            "type": "serial",
            "categoryField": "date_raw",
            "pathToImages": "http://www.amcharts.com/lib/3/images/",
            //"startDuration": 1,
            //"percentPrecision": 0,
            "fontFamily": "Lato",
            "fontSize": 14,
            //"precision": 0,
            "categoryAxis": {
                    "tickLength": 0,
                    "labelRotation": 36,
                    //"labelRotation": 36,
                    "parseDates": true,
                    "fontSize": 14,
                    "autoGridCount": true,
                    "centerLabelOnFullPeriod": true,
                    "minPeriod": "hh",
                    "labelOffset": 10,
            },
            "trendLines": [
            ],
            "graphs": [newGraph],
            //"guides": [],
            "valueAxes": [
                {
                    "maximum": 100,
                    "minimum": 0,
                    "gridAlpha": 0,
                    "axisAlpha": 0,
                    "fillAlpha": 0.05,
                    "fillColor": "#000000",
                    "tickLength": 0,
                    //"autoGridCount": false,
                    //"gridCount": 10,
                    "labelsEnabled": true,
                }
            ],
            "dataDateFormat": "YYYY-MM-DD",
            "allLabels": [{
                "text": "< Go Back",
                "bold": true,
                "x": 15,
                "y": 15,
                "size": 18,
                "align:": "left",
                "url": "#",
            }],
            //"balloon": {},
            "legend": {
                    "useGraphSettings": true,
                    "align": "center",
                    "borderAlpha": .25,
                    "valueText": "",
            },
            "titles": [
                {
                    "size": 22,
                    //"text": playerData.name+" ("+category+")",
                    "text": playerData.name,
                }
            ],
            "mouseWheelZoomEnabled": true,
            "chartCursor": {
                "cursorPosition": "mouse"   
            },
            "dataProvider": tempData,
            "export": getExportConfig(),
            "panEventsEnabled": false,
            "responsive": {
                "enabled": true,
                "rules": [
                    {
                        "maxWidth": 300,
                        "overrides": {
                            "categoryAxis": {
                                "labelRotation": 20,
                            }
                        }
                    },
                ],
            },
        });

        $('#timeline-scoring-chart tspan:contains("Go Back")').click(function() { 
            goBackPercentages();  
        });
    } else {
        //scoringLineChart.titles[0].text = playerData.name,
        scoringTimelineChart.graphs = [newGraph];
        scoringTimelineChart.dataProvider = tempData;
        //scoringLineChart.validateNow();
        scoringTimelineChart.validateData();
        scoringTimelineChart.animateAgain();
        $('#timeline-scoring-chart tspan:contains("Go Back")').click(function() { 
            goBackPercentages();  
        });
    }
}


function goBackPercentages() {
    scoringTimelineChart.destroy();
    scoringTimelineChart = null;
    $("#timeline-scoring-chart").hide();
    $("#timeline-scoring-toggles").hide();
    $("#scoring-chart").show();
    $("#scoring-toggles").show();
}

function goBackPlaymaking() {
    playmakingTimelineChart.destroy();
    playmakingTimelineChart = null;
    $("#timeline-playmaking-chart").hide();
    $("#timeline-playmaking-toggles").hide();
    $("#playmaking-chart").show();
    $("#playmaking-toggles").show();
}

function renderPlaymakingGraph(playerData){
    var timeRange = getTimeRangeForPlayer(playerData);
    var totals = JSON.parse(timeRange.totals);

    pushGraph(playerData, playmakingGraphs, function(s) {return s;});

    var activePlaymakingToggles = $(".stat-toggle-label.playmaking-toggle");
    for (var i=0; i<activePlaymakingToggles.length; i++){
        var toggle = $(activePlaymakingToggles[i]);
        var category = toggle.attr("data-category");
        var statKey = toggle.attr("data-value");
        var newPlayer = {};
        newPlayer[playerData.name] = totals[statKey];

        var exists = false;
        for(var j = 0;j < _playmakingData[category]["players"].length; j++){
            if (_playmakingData[category].players[j].hasOwnProperty(playerData.name)){
                _playmakingData[category].players[j][playerData.name] = totals[statKey]
                exists = true;
                break;
            }
        }
 
        if(!exists){
            _playmakingData[category]["players"].push(newPlayer);
        }

        if (toggle.hasClass("label-primary")){
            _playmakingData[category]["active"] = true;
        } else {
            _playmakingData[category]["active"] = false;
        }
    }

    tempData = [];
    for (var key in _playmakingData){
        if (_playmakingData[key].active){
            var data = {};
            data["category"] = key;
            for (var i=0;i<_playmakingData[key].players.length;i++){
                var player = _playmakingData[key].players[i];
                $.extend(data, player); 
            }

            tempData.push(data);
        }
    }


    if (playmakingChart === null){
        playmakingChart = AmCharts.makeChart("playmaking-chart",
            {
                "type": "serial",
                //"pathToImages": "http://cdn.amcharts.com/lib/3/images/",
                "categoryField": "category",
                "fontFamily": "Lato",
                "fontSize": 12,
                "startDuration": 1,
                "theme": "none",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "axisAlpha": 0,
                    "tickLength": 0,
                    "fontSize": 14,
                },
                "trendLines": [],
                "graphs": playmakingGraphs,
                "guides": [],
                "valueAxes": [
                    {
                        //"gridPosition": "start",
                        //"maximum": 100,
                        //"minimum": 0,
                        "gridAlpha": 0,
                        "axisAlpha": 0,
                        "fillAlpha": 0.05,
                        "fillColor": "#000000",
                        "tickLength": 0,
                        "autoGridCount": false,
                        "gridCount": 10,
                        "labelsEnabled": true,
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "legend": {
                    "useGraphSettings": true,
                    "align": "center",
                    "borderAlpha": .25,
                    //"data": [{title: "One", color: "#3366CC"},{title: "Two", color: "#EEEFFF"}]
                },
                "titles": [
                    {
                        "size": 22,
                        "text": "PLAYMAKING"
                    }
                ],
                "dataProvider": tempData,
                "export": getExportConfig(),
                "panEventsEnabled": false,
                "responsive": {
                    "enabled": true,
                    "rules": [
                        {
                            "maxWidth": 600,
                            "overrides": {
                                "categoryAxis": {
                                    "labelRotation": 80,
                                    "tickLength": 2,
                                    "fontSize": 12,
                                    "labelOffset": 10,
                                },
                                "valueAxes": {
                                    "fontSize": 0,
                                },
                            },
                        },
                    ],
                },
            });
        playmakingChart.addListener("clickGraphItem", function (event) {
            // let's look if the clicked graph item had any subdata to drill-down into
            // getting player id 
            var player_id = event.graph.id.substring(6);
            var category = event.item.category;
            TIMELINE_PLAYMAKING_SELECTED_PLAYER = player_id;
            //category_toggle.click();
            var selector = ".timeline-playmaking-toggle[data-category='" + category + "']";
            $('.timeline-playmaking-toggle').removeClass('label-primary').addClass('label-default');
            var category_toggle= $(selector).removeClass('label-default').addClass('label-primary');
            var category_value = $(selector).data('value');

            showTimelinePlaymakingView();
            renderTimelinePlaymakingGraph(player_id);
        });

    } else {
        playmakingChart.dataProvider = tempData;
        playmakingChart.graphs = playmakingGraphs;
        playmakingChart.validateData();
        playmakingChart.animateAgain();
    }
}

function renderPlayerGraphs(playerData){
    renderPercentagesGraph(playerData);
    //renderPLGraph(playerData);
    renderPlaymakingGraph(playerData);
}

