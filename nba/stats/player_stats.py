from nba.models import Player, Game, PlayerStats, Team, Season
from django.db.models import Avg, Sum
from django.core.cache import cache


def cache_full_stats_for_all_players():
    players = Player.objects.filter()
    for player in players:
        cache_full_season_stats_for_player(player, season=2014)
        cache_full_season_stats_for_player(player, season=2013)
        cache_full_season_stats_for_player(player)
        cache_game_stats_for_player(player, [15,5])


def cache_game_stats_for_player(player, number_of_games):
    current_season = Season.objects.get(year=2014)
    statlines = PlayerStats.objects.filter(player=player, minutes__gt=0, game__season=current_season).order_by('game__date').reverse()
    for games in number_of_games:
        stats = statlines[:games]
        totals = tallied_stats(stats)
        stats = pull_values(stats) 
            
        result = {
            'totals': totals,
            'timeline': list(stats)
        }

        stats_for_games_key = '{0}_stats_for_last_{1}_games'.format(player.unique_name, games)
        CACHE_EXPIRATION = 60 * 60 * 24 * 30
        cache.set(stats_for_games_key, result, CACHE_EXPIRATION)


def cache_full_season_stats_for_player(player, season=None):
    if season:
        statlines = PlayerStats.objects.filter(player=player, minutes__gt=0, game__season__year=season).order_by('game__date').reverse()
    else:
        statlines = PlayerStats.objects.filter(player=player, minutes__gt=0).order_by('game__date').reverse()

    totals = tallied_stats(statlines)
    stats = pull_values(statlines)

    result = {
        'totals': totals,
        'timeline': list(stats)
    }

    print result 

    CACHE_EXPIRATION = 60 * 60 * 24 * 30
    if season:
        full_season_key = '{0}_stats_for_{1}_season'.format(player.unique_name, season)
        cache.set(full_season_key, result, CACHE_EXPIRATION)
    else:
        all_time_key = '{0}_stats_for_all_time'.format(player.unique_name)
        cache.set(all_time_key, result, CACHE_EXPIRATION)
#TODO cache stats for current season


def tallied_stats(stats_queryset):
    totals = stats_queryset.annotate().aggregate(
        minutes=Avg('minutes'),
        shots_attempted=Sum('shots_attempted'),
        shots_made=Sum('shots_made'),
        field_goal_percentage=Avg('field_goal_percentage'),
        three_pointers_attempted=Sum('three_pointers_attempted'),
        three_pointers_made=Sum('three_pointers_made'),
        three_point_percentage=Avg('three_point_percentage'),
        free_throws_attempted=Sum('free_throws_attempted'),
        free_throws_made=Sum('free_throws_made'),
        free_throw_percentage=Avg('free_throw_percentage'),
        offensive_rebounds=Avg('offensive_rebounds'),
        defensive_rebounds=Avg('defensive_rebounds'),
        total_rebounds=Avg('total_rebounds'),
        assists=Avg('assists'),
        blocks=Avg('blocks'),
        steals=Avg('steals'),
        personal_fouls=Sum('personal_fouls'),
        turnovers=Sum('turnovers'),
        plus_minus=Avg('plus_minus'),
        points=Sum('points'),
        avg_points=Avg('points'),
        true_shooting_percentage=Avg('true_shooting_percentage'),
        effective_shooting_percentage=Avg('effective_shooting_percentage')
    )
    return totals
    

def pull_values(stats_queryset):
    values = stats_queryset.values(
        'minutes', 
        'game_label',
        'true_shooting_percentage',
        'effective_shooting_percentage',
        'field_goal_percentage', 
        'shots_attempted',
        'shots_made',
        'three_point_percentage', 
        'three_pointers_attempted',
        'three_pointers_made',
        'free_throw_percentage', 
        'free_throws_attempted', 
        'free_throws_made', 
        'offensive_rebounds', 
        'defensive_rebounds', 
        'total_rebounds',
        'assists', 
        'blocks', 
        'steals', 
        'plus_minus', 
        'points',
        'game__date',
        'game__post_season',
        'team',
        'player__name'
    )
    return values
