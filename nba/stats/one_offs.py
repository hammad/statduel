from nba.models import Player, Game, PlayerStats, Team, Season

def populate_percentages():
    #rename_unique()
    stats = PlayerStats.objects.filter(minutes__gt=0)
    print stats.count()
    for i,stat in enumerate(stats):
        print i
#        if not stat.field_goal_percentage:
#            stat.field_goal_percentage = divide(stat.shots_made, stat.shots_attempted) * 100
#            stat.save()
#        if not stat.three_point_percentage:
#            stat.three_point_percentage = divide(stat.three_pointers_made, stat.three_pointers_attempted) * 100
#            stat.save()
#        if not stat.free_throw_percentage:
#            stat.free_throw_percentage = divide(stat.free_throws_made, stat.free_throws_attempted) * 100
#            stat.save()
#        if stat.true_shooting_percentage == 0:
#            stat.true_shooting_percentage = true_shooting(stat)
#            stat.effective_shooting_percentage = effective_shooting(stat)
#            stat.save()
#        if not stat.game_label:
#            stat.game_label = game_label(stat)
#            stat.save()
        if stat.offensive_rebounds is None:
            stat.offensive_rebounds = 0
        if stat.defensive_rebounds is None:
            stat.defensive_rebounds = 0
        stat.total_rebounds = stat.offensive_rebounds + stat.defensive_rebounds
        stat.save()

def divide(num, den):
    num = float(num)
    den = float(den)
    if den is None or num is None or den == 0 or num == 0:
        return 0
    else:
        return float(num)/den

def true_shooting(stat):
    points = int(stat.points)
    shots_attempted = int(stat.shots_attempted)
    free_throws_attempted = int(stat.free_throws_attempted)

    try:
        true_shooting = (points * 100) / (2 *(shots_attempted + (.44 * free_throws_attempted) ) )
    except ZeroDivisionError:
        true_shooting = 0
    except TypeError:
        true_shooting = 0
    return true_shooting

def effective_shooting(stat):
    shots_made = int(stat.shots_made)
    three_pointers_made = int(stat.three_pointers_made)
    shots_attempted = int(stat.shots_attempted)

    try:
        effective_shooting = ((shots_made + (.5 * three_pointers_made) ) / shots_attempted) * 100
    except ZeroDivisionError:
        effective_shooting = 0
    except TypeError:
        effective_shooting = 0
    return effective_shooting

def game_label(stat):
    game = stat.game
    team = stat.team
    if team == game.home_team:
        label = "vs {away_team}".format(
            #month=game.date.month,
            #day=game.date.day,
            away_team=game.away_team.short_name.upper()
        )
    else:
        label = "@ {home_team}".format(
            #month=game.date.month, 
            #day=game.date.day, 
            home_team=game.home_team.short_name.upper()
        )
    return label

def rename_unique():
    players = Player.objects.all()
    for player in players:
        unique_name = player.unique_name
        try:
            player_id = unique_name.split('-')[-1]
            player_id = "1{0}".format(player_id)
            if player.unique_name != player_id:
                player.unique_name = player_id
                player.save()
        except IndexError:
            continue
