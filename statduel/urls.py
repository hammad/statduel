from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mano.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'nba.views.home', name='home'),
    url(r'nba/playerstats/(?P<players>[0-9]+(,[0-9]+)*)', 'nba.views.player_stats', name='player_stats'),
    url(r'nba/addplayer/(?P<unique_name>\d*)', 'nba.views.add_player', name='add_player'),
)
