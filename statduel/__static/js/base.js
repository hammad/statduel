var matcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        matches = [];

        substrRegex = new RegExp(q, 'i');

        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                matches.push({ value: str });
            }
        });

        cb(matches);
    };
}; 

var players = { 
        'Lebron James': 'lebron-james-1966',
        'Kevin Durant': 'kevin-durant-3202'
};

console.log("HELLO");

$("#typeahead").typeahead(
{
    hint: true,
    highlight: true,
    minLength: 1,
    autoselect: true,
},
{
    name: 'players',
    source: matcher(Object.keys(players)),
    displayKey: 'value'
});

$('#typeahead').keypress(function (e) {
    if (e.which == 13) {
        console.log('clicked ENTER!');
        $('#search-button').click();
        return true;
    }
});


$("#search-button").on('click', function (e) {
    var searchTerm = $("#typeahead").typeahead('val');
    var uniqueId = players[searchTerm];
    console.log(uniqueId);
    var url = "http://192.241.185.245:9091/nba/playerstats/?unq=" + uniqueId;
    window.open(url, '_self', false);
    //$.get( "http://192.241.185.245:9091/stats/nba/", { unq: uniqueId } )
    //    .done(function (data) {
    //        var stats = JSON.parse(data);
    //        //var playerTab = $( "<div/>", { "class": "col-lg-3 center"}).css("display", "none").appendTo( "#player-row" );
    //        //playerTab.html("Kevin Durant<br>Position: SF<br>Age:27");
    //        //playerTab.toggle("slide", {direction: "right"}, 500);
    //        //var playerTab = $( "<div/>", { "class": "col-lg-3 center"}).css("display", "none").appendTo( "#player-row" );
    //        //playerTab.html("LEBRON JAMES<br>Position: PF<br>Age:28");
    //        //playerTab.toggle("slide", {direction: "right"}, 500);
    //        var percentagesChart = new CanvasJS.Chart("percentages-chart",
    //            {
    //                title: { text: "Percentages" },
    //                data: [
    //                    {
    //                        type: "column",
    //                        name: stats.info.name,
    //                        dataPoints: [
    //                            { label: "Field Goals", y: stats.scoring.field_goals.field_goal_percentage },
    //                            { label: "3 Pointers", y: stats.scoring.three_pointers.three_pointer_percentage },
    //                            { label: "Free Throws", y: stats.scoring.free_throws.free_throw_percentage },
    //                        ],
    //                    },
    //                ],
    //            });
    //        percentagesChart.render();
    //        var playMakingChart = new CanvasJS.Chart("playmaking-chart",
    //            {
    //                title: { text: "Play Making" },
    //                data: [
    //                    {
    //                        type: "column",
    //                        name: stats.info.name,
    //                        dataPoints: [
    //                            { label: "Assists", y: stats.assists },
    //                            { label: "Blocks", y: stats.blocks },
    //                            { label: "Steals", y: stats.steals },
    //                        ],
    //                    },
    //                    //{
    //                    //    type: "stackedColumn",
    //                    //    name: stats.info.name,
    //                    //    dataPoints: [
    //                    //        { label: "Rebounds", y: stats.rebounds.offensive_rebounds },
    //                    //    ],
    //                    //},
    //                ],
    //            });
    //        playMakingChart.render();

    //        //var statKeys = Object.keys(stats);
    //        //var graphData = [];
    //        //for (var i = 0; i < statKeys.length; i++){
    //        //    var key = statKeys[i];
    //        //    if (key === 'scoring'){
    //        //        
    //        //    }


    //        //    //var key = statKeys[i];
    //        //    //var value = stats[key];
    //        //    //console.log(i);
    //        //    //console.log(key);
    //        //    //console.log(value);
    //        //    //
    //        //    //var col = $( "<div/>", { "class": "col-lg-4"}).appendTo( "#graph-row" );
    //        //    //$( "<div/>", { "id": "charContainer-" + i, "height": "270px"}).appendTo(col);
    //        //    //var chart = new CanvasJS.Chart("charContainer-" + i, {
    //        //    //    data: [
    //        //    //        {
    //        //    //            type: "column",
    //        //    //            dataPoints: [
    //        //    //                {
    //        //    //                    label: key, y: value
    //        //    //                }
    //        //    //            ]
    //        //    //        }
    //        //    //    ]
    //        //    //});
    //        //    //chart.render();
    //        //    //graphData.push({ label: key, y: value });
    //        //}
    //        $("#stats-area").fadeIn().removeClass('hidden');
    //        //var chart = new CanvasJS.Chart("charContainer", {
    //        //    data: [
    //        //        {
    //        //            type: "column",
    //        //            dataPoints: graphData 
    //        //        }
    //        //    ]
    //        //});
    //        //chart.render();
    //    });
});
console.log("GOODBYE");

